package com.demo.shop;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.persistence.EntityNotFoundException;

import com.demo.shop.constant.StatusConstant;
import com.demo.shop.entity.Category;
import com.demo.shop.entity.Notification;
import com.demo.shop.entity.Product;
import com.demo.shop.repository.NotificationRepository;
import com.demo.shop.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

@SpringBootApplication
public class ShopBeRestApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ShopBeRestApiApplication.class, args);
	}
	@Autowired
	ProductRepository productRepository;
	@Autowired
	private NotificationRepository notificationRepository;
	@Scheduled(fixedRate = 300000)
	void someJob() throws Exception {
		List<Product> list = productRepository.findByStatus(StatusConstant.STATUS_ACTIVE);

		for (Product produt : list) {
			Notification notification = new Notification();
			Date dateNow = new Date();
			Date expired = produt.getExpired();
			long millis = expired.getTime(); // lay ngay het han
			long getDiff = millis - dateNow.getTime(); // tinh thoi gian
			long getDaysDiff = TimeUnit.MILLISECONDS.toMinutes(getDiff);
			long hours = TimeUnit.MILLISECONDS.toHours(getDiff);
			if (getDaysDiff>0 && getDaysDiff < 2875) {
				notification.setDescriptions("Sản phẩm " + produt.getProductName() + " còn " + hours + " giờ là hết hạn");
				notification.setStorageTime((int) getDaysDiff);
				notification.setCreateDate(dateNow);
				notification.setProductId(produt.getId());
				Product p = productRepository.findById(notification.getProductId()).get();
				notification.setProduct(p);
				notificationRepository.save(notification);
			}
			if(produt.getQuantity() <= 10) {
				Notification notification1 = new Notification();
				notification1.setDescriptions("Sản phẩm " + produt.getProductName() + " sắp hết");
//				notification.setStorageTime((int) getDaysDiff);
				notification1.setCreateDate(dateNow);
				notification1.setProductId(produt.getId());
				Product p = productRepository.findById(notification1.getProductId()).get();
				notification1.setProduct(p);
				notificationRepository.save(notification1);
			}
		}

	}
}
@Configuration
@EnableScheduling
@ConditionalOnProperty(name = "scheduling.enabled", matchIfMissing = true)
class SchedulingConfiguration {

}

