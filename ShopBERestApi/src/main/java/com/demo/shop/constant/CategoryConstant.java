package com.demo.shop.constant;

public class CategoryConstant {
	public static final int Frozen_Food = 3;
	public static final int Fruit = 2;
	public static final int Fresh_Vegetable = 1;
}
