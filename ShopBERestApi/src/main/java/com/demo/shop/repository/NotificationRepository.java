package com.demo.shop.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.demo.shop.entity.Notification;

@Repository
public interface NotificationRepository extends JpaRepository<Notification, Integer> {

	@Query(value = "select n.id, n.descriptions, n.create_date, n.product_id, n.storage_time, n.update_date   "
			+ "from product p inner join notification n on p.id = n.product_id "
			+ "WHERE p.status = 1 "
			+ "group by n.descriptions "
			+ "order by n.create_date desc", nativeQuery = true)
	List<Notification> findNotificationByDate();

	@Query(value = "select n.id, n.product_id, n.storage_time, n.create_date, n.update_date, n.descriptions from notification n inner join product p on n.product_id = p.id where n.id = :id and p.status = 1", nativeQuery = true)
	Optional<Notification> findById(Integer id);
}
