package com.demo.shop.repository;

import com.demo.shop.entity.Order;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRepository extends JpaRepository<Order, Integer> {
    Order findByUserIdAndPaymentId(Integer userId, String paymentId);

    Order findByUserIdAndToken(Integer userId, String token);

    @Query(value = "select COALESCE(SUM(total_price),0) as tolalPrice " + "FROM `order` o2 "
            + "where o2.status = :status AND DAY(created_date) = DAY(curdate());", nativeQuery = true)
    Integer revenueByDay(Integer status);

    @Query(value = "select COALESCE(SUM(total_price),0) as tolalPrice " + "FROM `order` o2 "
            + "where o2.status = :status AND WEEK(created_date) = WEEK(curdate());", nativeQuery = true)
    Integer revenueByWeek(Integer status);

    @Query(value = "select COALESCE(SUM(total_price),0) as tolalPrice " + "FROM `order` o2 "
            + "where o2.status = :status AND MONTH(created_date) = MONTH(curdate());", nativeQuery = true)
    Integer revenueByMonth(Integer status);

    @Query(value = "select COALESCE(SUM(total_price),0) as tolalPrice " + "FROM `order` o2 "
            + "where o2.status = :status AND YEAR(created_date) = YEAR(curdate());", nativeQuery = true)
    Integer revenueByYear(Integer status);

    @Query(value = "select * "
            + "from `order`"
            + "where status = :status AND Day(created_date) = Day(curdate());", nativeQuery = true)
    List<Order> getListOrderByDay(Integer status);

    @Query(value = "select * "
            + "from `order` "
            + "where status = :status AND Week(created_date) = Week(curdate());", nativeQuery = true)
    List<Order> getListOrderByWeek(Integer status);

    @Query(value = "select * "
            + "from `order`"
            + "where status = :status AND month(created_date) = month(curdate());", nativeQuery = true)
    List<Order> getListOrderByMonth(Integer status);

    @Query(value = "select * "
            + "from `order`"
            + "where status = :status AND year(created_date) = year(curdate());", nativeQuery = true)
    List<Order> getListOrderByYear(Integer status);

}
