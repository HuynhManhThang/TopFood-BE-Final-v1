package com.demo.shop.request;

import lombok.Data;

@Data
public class UpdateOrderRequest {

	private Integer status;
}
