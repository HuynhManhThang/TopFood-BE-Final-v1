package com.demo.shop.response;

public class ExcelResponse {
	private String status;

	
	
	public ExcelResponse(String status) {
		this.status = status;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	
}