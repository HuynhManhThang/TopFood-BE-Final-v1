package com.demo.shop.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.shop.entity.Notification;
import com.demo.shop.repository.NotificationRepository;
import com.demo.shop.response.NotificationResponse;
import com.demo.shop.service.NotificationService;

@Service
public class NotificationServiceImpl implements NotificationService {

	@Autowired
	private NotificationRepository notificationRepository;

	@Override
	public List<NotificationResponse> getAll() {
		List<Notification> list = notificationRepository.findNotificationByDate();
		List<NotificationResponse> notificationResponses = new ArrayList<>();
		for (Notification notification : list) {
			NotificationResponse response = new NotificationResponse();
			response.setId(notification.getId());
			response.setProduct(notification.getProduct());
			response.setStorageTime(notification.getStorageTime());
			response.setDescription(notification.getDescriptions());
			response.setUpdateDate(notification.getUpdateDate());
			response.setCreateDate(notification.getCreateDate());
			notificationResponses.add(response);
		}
		return notificationResponses;
	}


	@Override
	public NotificationResponse findById(Integer id) {
		NotificationResponse notificationResponse = new NotificationResponse();
		Optional<Notification> notification = notificationRepository.findById(id);
		if (notification != null) {
			notificationResponse.setId(notification.get().getId());
			notificationResponse.setProduct(notification.get().getProduct());
			notificationResponse.setStorageTime(notification.get().getStorageTime());
			notificationResponse.setDescription(notification.get().getDescriptions());
			notificationResponse.setUpdateDate(notification.get().getUpdateDate());
			notificationResponse.setCreateDate(notification.get().getCreateDate());
		}
		return notificationResponse;
	}

	
}
